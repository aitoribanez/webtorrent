var torrent = require('./torrent')

window.onload = function() {

	torrent.downloadTorrent ('magnet:?xt=urn:btih:6a9759bffd5c0af65319979fb7832189f4f3c35d')

};

var util = require('./util');
var thunky = require('thunky');
var xhr = require('xhr');
var debug = require('debug');
var getClient = thunky(function (cb) {
	console.log("START getCLient")
  	getRtcConfig('https://instant.io/rtcConfig', function (err, rtcConfig) {
		if (err) util.error(err)
		createClient(rtcConfig)
	})

	function createClient (rtcConfig) {
	    console.log("CLIENT has been created");

	    var client = window.client = new WebTorrent({ rtcConfig: rtcConfig })
	    client.on('warning', util.warning)
	    client.on('error', util.error)
	    cb(null, client)
	}

	function getRtcConfig (url, cb) {
		console.log("Getting RTC config");

		xhr(url, function (err, res) {
		    if (err || res.statusCode !== 200) {
		    	cb(new Error('Could not get WebRTC config from server. Using default (without TURN).'))
		    } else {
		    	var rtcConfig
		     	try {
		        	rtcConfig = JSON.parse(res.body)
		      	} catch (err) {
		        	return cb(new Error('Got invalid WebRTC config from server: ' + res.body))
		      	}
		    	debug('got rtc config: %o', rtcConfig)
		    	cb(null, rtcConfig)
			}	
		})
	}
})
